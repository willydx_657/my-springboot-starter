package com.wujunshen.elasticsearch5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleElasticsearch5SpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleElasticsearch5SpringBootStarterApplication.class, args);
    }
}
