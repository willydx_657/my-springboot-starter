# elasticsearch6.x版本

由于目前es官方5.x和6.x两个版本同时在维护，所以我们也同时提供两个版本的starter

#### 项目介绍
参见elasticsearch6-spring-boot-starter项目
在/resources/META_INF/spring.factories文件中指定自动装配类

Elasticsearch6AutoConfiguration是自动装配类
装载es的TransportClient类和自定义的Elasticsearch6Template类

#### 使用说明
参见sample-elasticsearch6-spring-boot-starter项目
1. 在pom文件中加入starter依赖
        
```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>elasticsearch6-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
    <dependency>
        <groupId>org.elasticsearch</groupId>
        <artifactId>elasticsearch</artifactId>
        <version>6.2.3</version>
    </dependency>
```

2. 在application.properties配置文件加入下列内容

```
    es.host=localhost
    es.port=9300
    es.httpPort=9200
    es.clusterName=elasticsearch-test
    es.sniff=false
    es.transport.ignoreClusterName=true
    es.transport.pingTimeout=5s
    es.transport.nodesSamplerInterval=5s
```

其中前4行必填，其它可填可不填

3. 启动sample-elasticsearch6-spring-boot-starter应用后，运行test包下的ESCreateTest和ESSearchTest两个测试类

如下图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0706/161555_f254cda0_43183.png "Snip20180706_1.png")