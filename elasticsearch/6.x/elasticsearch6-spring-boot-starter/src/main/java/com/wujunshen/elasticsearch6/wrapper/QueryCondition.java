package com.wujunshen.elasticsearch6.wrapper;

import lombok.Data;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.QueryBuilder;

/**
 * 查询条件类
 */
@Data
public class QueryCondition {
    //毫秒数
    private long millis = 1000;
    //查询条数
    private int size = 50;
    //传入的查询条件构造器
    private QueryBuilder queryBuilder;
    //搜索键值对，要搜索的字段和其对应的搜索内容
    private QueryPair queryPair;
    //搜索类型
    private SearchType searchType = SearchType.DFS_QUERY_THEN_FETCH;
}