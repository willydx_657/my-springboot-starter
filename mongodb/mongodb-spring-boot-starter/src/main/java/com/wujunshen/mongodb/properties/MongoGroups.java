package com.wujunshen.mongodb.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/24 <br>
 * Time:  14:41 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
@ConfigurationProperties("spring.data.mongodb")
public class MongoGroups implements Validator {
    private List<MongoGroup> groups;

    @Override
    public boolean supports(final Class<?> clazz) {
        return MongoGroups.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors) {
        // nothing to validate
    }
}