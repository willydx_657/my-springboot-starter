# Orika

#### 项目介绍

参见orika-spring-boot-starter项目

在/resources/META_INF/spring.factories文件中指定自动装配类

OrikaAutoConfiguration是自动装配类。

#### 使用说明

参见sample-orika-spring-boot-starter项目

1. 在pom文件中加入starter依赖

```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>orika-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
```

2. 在application.properties文件中如有必要可加入相关属性，例如如下：

```
    orika.map-nulls=false
    orika.json[0].convert-name=bookInfoConvert
    orika.json[0].class-name=com.wujunshen.sample.dest.BookInfo
    orika.enumeration[0].convert-name=bookTypeConvert
    orika.enumeration[0].class-name=com.wujunshen.sample.dest.BookType
```

这里参见com.wujunshen.orika.properties.OrikaProperties属性配置类，所有属性缺省为true，因此隐式和显式设置为true效果是一样的。只有需要显式设置为false时，可在application.properties文件中配置

设置orika.map-nulls=false后，参见com.wujunshen.sample.UnitTest类中givenSrcWithNullAndGlobalConfigForNoNull_whenFailsToMap_ThenCorrect和givenSrcWithNullAndGlobalConfigForNull_whenFailsToMap_ThenCorrect方法及其注释，并运行查看效果

另外介绍缺省的4种类型转换器
参见com.wujunshen.orika.properties.OrikaProperties属性配置类可看见

```
    private String valueConvert = "valueConvert";

    private String dateConvert = "dateConvert";
```

这里valueConvert转换器是将类中Object类型属性变量和String类型属性变量进行互相转换，缺省转换器名为"valueConvert"，也可在application.properties文件中设置

```
    orika.valueConvert=XXX
```

来覆盖替换缺省转换器名

参见com.wujunshen.sample.UnitTest类中givenSrcWithObjectField_whenMapsThenCorrect和givenReverseSrcWithObjectField_whenMapsThenCorrect方法及其注释，并运行查看效果

dateConvert转换器是将类中java.util.Date类型属性变量和日期格式为"yyyy-MM-dd HH:mm:ss"的String类型属性变量进行互相转换
缺省转换器名为"dateConvert"，，也可在application.properties文件中设置

```
    orika.dateConvert=XXX
```

来覆盖替换缺省转换器名

参见com.wujunshen.sample.UnitTest类中givenSrcWithDateField_whenMapsThenCorrect和givenReverseSrcWithDateField_whenMapsThenCorrect方法及其注释，并运行查看效果

另外application.properties文件
```
    orika.json[0].convert-name=bookInfoConvert
    orika.json[0].class-name=com.wujunshen.sample.dest.BookInfo
```

是设置jsonConvert转换器，将类中具体实体类型属性变量，使用spring中的Jackson框架转换成JSON字符串类型属性变量，也可互相转换

由于需要指定具体的实体类型对象，才能进行JSON转换，因此该转换器没有缺省配置，必须显式在application.properties文件中设置。如上代码，转换器名为"bookInfoConvert"，具体实体类型对象全路径名为com.wujunshen.sample.dest.BookInfo

最后一个是enumConvert转换器，application.properties文件中内容如下

```
    orika.enumeration[0].convert-name=bookTypeConvert
    orika.enumeration[0].class-name=com.wujunshen.sample.dest.BookType
```

它是将类中枚举类型属性变量和代表其code的Integer类型属性变量进行互相转换

和jsonConvert同样理由，需要指定具体的枚举类型对象，因此没有缺省配置，必须显式在application.properties文件中设置。如上代码，转换器名为"bookTypeConvert"，具体实体类型对象全路径名为com.wujunshen.sample.dest.BookType

参见com.wujunshen.sample.UnitTest类中givenSrcWithJSONAndNumField_whenMapsThenCorrect和givenReverseSrcWithJSONAndNumField_whenMapsThenCorrect方法及其注释，并运行查看效果

注意:jsonConvert和enumConvert都可以配置多个要转换成JSON字符串的实体类和枚举类

只要按照application.properties文件中内容格式，依次以orika.json\[0\].xxx、orika.json\[1\].xxx或orika.enumeration\[0\].xxx、orika.enumeration\[1\].xxx这样指定转换器名和实体类，枚举类即可

3. 启动sample-orika-spring-boot-starter应用后，运行test包下UnitTest类中各方法

具体使用和讲解可见http://www.baeldung.com/orika-mapping

这是目前个人认为最全面的Orika使用方法介绍