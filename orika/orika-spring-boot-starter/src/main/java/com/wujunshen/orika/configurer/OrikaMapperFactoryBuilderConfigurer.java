package com.wujunshen.orika.configurer;

import ma.glasnost.orika.impl.DefaultMapperFactory.MapperFactoryBuilder;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/30 <br>
 * Time:  23:56 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
public interface OrikaMapperFactoryBuilderConfigurer {
    /**
     * Configures the {@link MapperFactoryBuilder}.
     *
     * @param orikaMapperFactoryBuilder the {@link MapperFactoryBuilder}.
     */
    void configure(MapperFactoryBuilder<?, ?> orikaMapperFactoryBuilder);
}