package com.wujunshen.orika.convert;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  15:50 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
public class EnumConvert<T extends Enum<T>> extends BidirectionalConverter<T, Integer> {
    @Override
    public Integer convertTo(T source, Type<Integer> destinationType, MappingContext mappingContext) {
        return source.ordinal() + 1;
    }

    @Override
    public T convertFrom(Integer source, Type<T> destinationType, MappingContext mappingContext) {
        return destinationType.getRawType().getEnumConstants()[source - 1];
    }
}