package com.wujunshen.orika.properties;

import lombok.Data;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  23:38 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
public class EnumConvertProperties {
    private String convertName;

    private String className;
}