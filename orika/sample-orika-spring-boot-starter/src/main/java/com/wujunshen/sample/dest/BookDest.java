package com.wujunshen.sample.dest;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/31 <br>
 * Time:  01:15 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
@AllArgsConstructor
public class BookDest {
    // 一个枚举类型
    private BookType bookType;

    // 一个类包含 ISBN 和 page
    private BookInfo bookInfo;
}