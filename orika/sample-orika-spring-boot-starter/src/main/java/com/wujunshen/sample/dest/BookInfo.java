package com.wujunshen.sample.dest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/31 <br>
 * Time:  01:16 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
public class BookInfo {
    @JsonProperty("ISBN")
    private String isbn;
    @JsonProperty("page")
    private int page;
}