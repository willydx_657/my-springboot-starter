package com.wujunshen.sample.dest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Dest {
    private String name;
    private int age;
}