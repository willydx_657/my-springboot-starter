package com.wujunshen.sample.source;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class PersonNameMap {
    private Map<String, String> nameMap;
}