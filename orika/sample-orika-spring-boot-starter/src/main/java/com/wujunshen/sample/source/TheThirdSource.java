package com.wujunshen.sample.source;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  19:07 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
@AllArgsConstructor
public class TheThirdSource {
    private String anotherName;
    private int anotherAge;

    public TheThirdSource(int anotherAge) {
        this.anotherAge = anotherAge;
    }
}