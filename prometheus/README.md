# prometheus

#### 项目介绍
参见prometheus-spring-boot-starter项目
在/resources/META_INF/spring.factories文件中指定所需访问的两个类

PrometheusAutoConfiguration是自动装配类。
无需在自己应用里使用普罗米修斯的annotation注解就可查看其EndPoint信息

PrometheusSpringApplicationRunListener是监听器
若有需要，可在application.properties里设定management.port端口，走自定义的端口

若无需要，则无需配置management.port端口，统一使用8081端口

#### 使用说明
参见sample-prometheus-spring-boot-starter项目
1. 在pom文件中加入starter依赖

```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>prometheus-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
```

2. 启动sample-prometheus-spring-boot-starter应用后，运行[http://localhost:8007/hello](http://localhost:8007/hello)

查看sample-prometheus-spring-boot-starter应用是否能正常工作

3. 运行[http://localhost:8081/prometheus](http://localhost:8081/prometheus)和[http://localhost:8081/actuator](http://localhost:8081/actuator)

查看监控端点是否能正常显示信息

4. 假设在application.properties里设定

```
    management.port=9081
```

则启动sample-prometheus-spring-boot-starter应用后，运行[http://localhost:9081/prometheus](http://localhost:9081/prometheus)和[http://localhost:9081/actuator](http://localhost:9081/actuator)

查看监控端点是否能正常显示信息

如下列这些图

![输入图片说明](https://gitee.com/uploads/images/2018/0616/013841_6dbb7ca6_43183.png "Snip20180616_2.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0616/013939_6e471229_43183.png "Snip20180616_3.png")