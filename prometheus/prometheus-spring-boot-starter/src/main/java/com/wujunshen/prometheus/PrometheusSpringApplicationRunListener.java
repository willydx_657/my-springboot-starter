package com.wujunshen.prometheus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;

import java.util.Properties;

public class PrometheusSpringApplicationRunListener implements SpringApplicationRunListener {
    private static final String SPRINGBOOT_MANAGEMENT_ENABLED_KEY = "management.security.enabled";
    private static final boolean SPRINGBOOT_MANAGEMENT_ENABLED_VALUE = false;

    private static final String SPRINGBOOT_MANAGEMENT_PORT_KEY = "management.port";
    private static final int SPRINGBOOT_MANAGEMENT_PORT_VALUE = 8081;

    public PrometheusSpringApplicationRunListener(SpringApplication application, String[] args) {
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment env) {
        Properties props = new Properties();
        props.put(SPRINGBOOT_MANAGEMENT_ENABLED_KEY, SPRINGBOOT_MANAGEMENT_ENABLED_VALUE);
        props.put(SPRINGBOOT_MANAGEMENT_PORT_KEY, SPRINGBOOT_MANAGEMENT_PORT_VALUE);
        env.getPropertySources().addLast(new PropertiesPropertySource("application", props));
    }

    @Override
    public void finished(ConfigurableApplicationContext context, Throwable exception) {
    }

    @Override
    public void starting() {
    }
}