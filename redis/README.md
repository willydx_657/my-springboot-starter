# Redis

#### 项目介绍

此项目适用于redis集群环境，且覆盖原有springboot官方redis-starter配置，因此如果是单机redis不适用此自定义starter

参见redis-spring-boot-starter项目
在/resources/META_INF/spring.factories文件中指定自动装配类

MyRedisConfiguration是自动装配类

手工构建redis客户端jedis的集群配置信息

并且使用StringRedisSerializer来序列化和反序列化redis的key值

使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值

且不支持原有springboot官方redis-starter配置（以spring.redis为前缀的配置）

#### 使用说明

参见sample-redis-spring-boot-starter项目

1. 在pom文件中加入starter依赖
        
```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>redis-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
```

2. 在application.properties配置文件加入下列内容

```
    redis.cluster.nodes=localhost:7000,localhost:7001,localhost:7002,localhost:7003,localhost:7004,localhost:7005
    redis.cluster.max-redirects=6
    redis.cluster.password=
    redis.cluster.time-out=20000
    redis.pool.max-wait=1000
    redis.pool.max-active=8
    redis.pool.max-idle=8
    redis.pool.min-idle=0
```

其中前2行必填，其它可填可不填

3. 启动sample-redis-spring-boot-starter应用后，运行test包下的RedisConfigTest和RedisTest两个测试类

如下图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0712/134744_e89b6736_43183.png "Snip20180711_1.png")