package com.wujunshen.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleRedisSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleRedisSpringBootStarterApplication.class, args);
    }
}
