# SnowFlake

#### 项目介绍

此项目封装了雪花算法id生成细节，只需依赖注入IdService接口类就可使用

参见snowflake-spring-boot-starter项目
在/resources/META_INF/spring.factories文件中指定自动装配类

SnowFlakeConfiguration是自动装配类

#### 使用说明

参见sample-snowflake-spring-boot-starter项目

1. 在pom文件中加入starter依赖

```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>snowflake-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
```

2. 在application.properties配置文件加入下列内容

```
    generate.worker=1021
```

指定workerID即可

3. 启动sample-snowflake-spring-boot-starter应用后，运行test包下的IdControllerTest测试类。

也可打开浏览器或postman，restclient等工具调用web接口查看

如下图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/163303_6424b8ed_43183.png "Snip20180806_1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/163328_50cdbe88_43183.png "Snip20180806_2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/163348_f7d9b144_43183.png "Snip20180806_3.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/163405_0f5ccfd0_43183.png "Snip20180806_4.png")