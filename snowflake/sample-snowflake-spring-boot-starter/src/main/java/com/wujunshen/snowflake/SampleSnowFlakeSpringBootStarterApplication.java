package com.wujunshen.snowflake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleSnowFlakeSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleSnowFlakeSpringBootStarterApplication.class, args);
    }
}
