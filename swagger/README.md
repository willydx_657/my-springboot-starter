# swagger

#### 项目介绍
参见swagger-spring-boot-starter项目

在/resources/META_INF/spring.factories文件中指定配置类SwaggerAutoConfiguration

然后在应用里加上swagger的相关配置就可访问swagger的web界面。

为了保证部署到生产环境之后的安全性问题，可通过swagger.enable=false关闭swagger访问权限

#### 使用说明
参见sample-swagger-spring-boot-starter项目

1. 在pom文件中加入starter依赖
        
```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>swagger-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
```

2. 在application.properties文件中加入下列配置

```
    swagger.enable=true
    swagger.contextPath=/
    swagger.contact=如有任何疑问，请联系我
    swagger.version=V2.0 
```

3. 启动sample-swagger-spring-boot-starter应用后，运行[http://localhost:8012/swagger-ui.html](http://localhost:8012/swagger-ui.html)查看sample-swagger-spring-boot-starter应用是否能正常工作

如下列这些图
![输入图片说明](https://gitee.com/uploads/images/2018/0622/165647_0ba066f3_43183.png "Snip20180622_15.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0622/165733_ed6527af_43183.png "Snip20180622_16.png")